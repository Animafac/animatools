## ANIMASPACE

<!-- ![Animaspace Logo](<link_to_logo>) -->

Animaspace est un espace de cloud partagé basé sur la solution collaborative libre Nextcloud.
Animaspace est conçu pour permettre aux associations adhérentes d'Animafac de gérer en ligne,
de manière libre et gratuite, divers types de fichiers et projets.

## Fonctionnalités

### Stockage et Partage de Fichiers

- Téléchargez et stockez des fichiers de tous types en toute simplicité.
- Partagez des fichiers avec d'autres membres de votre association.

### Gestion de Projets (Méthode Kanban)

- Organisez vos projets en utilisant la méthode Kanban
  pour une visualisation claire des étapes et des tâches.

### Édition Collaborative de Documents Bureautiques

- Éditez des documents texte, des feuilles de calcul, des présentations, 
  directement en ligne.
- Travaillez simultanément avec d'autres membres de votre association
  sur les mêmes documents.

## Demande d'Espace

Pour bénéficier de votre espace de stockage partagé dans Animaspace,
vous devez vous pré inscrire en remplissant [ce formulaire](https://forms.animafac.net/index.php/943299?lang=fr)


Si vous avez des questions ou si vous avez besoin d'aide, n'hésitez pas à contacter 
le support technique d'Animafac à [numerique@animafac.net](mailto:numerique@animafac.net).
