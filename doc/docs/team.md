# [![Animateam](resources/team/logo.svg)](https://team.animafac.net/)

## Nommer son compte correctement

1. Cliquez sur le menu principal d'Animateam.
2. Sélectionnez *Profil* > *Paramètres du profil*.
3. Dans *Paramètres du profil*, choisissez *Nom complet*.
4. Saisissez votre Nom et prénom.
5. Dans la section *Pseudo*, indiquez votre nom de famille suivi de votre
   fonction ou association entre parenthèses (exemples : Legault (directrice) ou
   Leclerc (Potatoit)).
6. Enregistrez.

## Afficher correctement les noms des autres utilisateurs

1. Cliquez sur le bouton *Paramètres* situé en haut à droite d'Animateam.
2. Choisissez *Affichage*.
3. Cliquez sur *Affichage des membres de l'équipe*.
4. Sélectionnez *Afficher prénom et nom*.
5. Enregistrez.

## Afficher le statut d'activité sur la photo de profil

1. Cliquez sur le bouton *Paramètres* situé en haut à droite d'Animateam.
2. Choisissez *Affichage*.
3. Cliquez sur *Afficher le statut d'activité sur la photo de profil*.
4. Sélectionnez *Activé*.
5. Enregistrez.

## Modifier son mot de passe

1. Cliquez sur l'icône en haut à droite d'Animateam (représentant une lettre
   ou une photo de profil).
2. Sélectionnez *Profil*.
3. Dans *Sécurité* > *Mot de passe*, cliquez sur *Modifier*.
4. Remplissez tous les champs requis.
5. Enregistrez.

## Télécharger l'application pour ordinateur/smartphone

### Ordinateur

1. Visitez le site officiel de Mattermost en utilisant ce lien :
   [Télécharger Mattermost](https://mattermost.com/download/?redirect_source=about-mm-com).
2. Une fois sur la page de téléchargement, sélectionnez la version compatible
   avec votre système d'exploitation (Windows, Mac, Linux).
3. Si l'application vous demande l’URL du serveur,
    entrez [l'url d'animateam](https://team.animafac.net/).

**Utilisateurs de Google Chrome sur ordinateur**

1. Ouvrez Chrome.
2. Accédez au site web Mattermost que vous souhaitez utiliser.
3. Cliquez sur l'icône des trois points verticaux en haut à droite de la fenêtre
  de Chrome pour accéder au menu.
4. Survolez l'option *Plus d'outils* dans le menu déroulant, puis sélectionnez
  *Créer un raccourci*.
5. Vous pouvez choisir de conserver le nom par défaut ou de le personnaliser.
6. Cochez la case *Ouvrir sous forme de fenêtre* pour que le site s'ouvre comme
  une application autonome.
7. Cliquez sur *Créer* pour générer le raccourci. Une icône Mattermost apparaîtra
  sur votre bureau pour un accès rapide.

### Smartphone 

1. Pour Android : Accédez au Google Play Store, recherchez **Mattermost** et
  installez l'application officielle.
2. Pour iOS (iPhone/iPad) : Accédez à l'App Store, recherchez **Mattermost** et
  installez l'application officielle.

**Utilisateurs de Chrome sur smartphone**

1. Ouvrez Chrome sur votre smartphone.
2. Accédez au site web Mattermost.
3. Cliquez sur le menu principal de Chrome (les trois points verticaux).
4. Sélectionnez *Ajouter à l'écran d'accueil*.
5. Une icône Mattermost sera ajoutée à votre écran d'accueil pour un accès rapide.

## Utiliser les couleurs de Slack

1. Cliquez sur le bouton *Paramètres* situé en haut à droite d'Animateam.
2. Sélectionnez *Paramètres du compte*.
3. Dans *Affichage*, cliquez sur *Thème*.
4. Choisissez *Importer des couleurs de thème depuis Slack*.
5. Copiez le texte suivant dans le champ :
   `#4D394B,#3E313C,#4C9689,#FFFFFF,#3E313C,#FFFFFF,#38978D,#EB4D5C`.
6. Cliquez sur *Envoyer*.

## Activation des messages indiquant qu’un utilisateur a rejoint/quitté le canal

1. Cliquez sur le menu principal d'Animateam.
2. Sélectionnez *Paramètres*.
3. Dans *Options avancées*, cliquez sur
   *Activation des messages indiquant qu’un utilisateur a rejoint/quitté le
   canal*.
4. Cliquez sur **Enregistrer**.

## Poster un GIF animé

1. Tapez **/giphy** suivi d'un mot-clé.
2. Envoyez le message.
3. Animateam remplacera votre message par un GIF en rapport avec le mot-clé
   choisi.

## Inviter un nouveau membre dans l'équipe

   **Pour envoyer une invitation par e-mail**

   1. Cliquez sur l’icone **+** en haut à gauche d'Animateam.
   2. Sélectionnez *Inviter des personnes*.
   3. Remplissez l'adresse e-mail et le nom de la personne.
   4. Cliquez sur *Envoyer*.
   (Le lien dans l'e-mail sera temporaire.)

   **Pour envoyer manuellement l'invitation**

   1. Cliquez sur l’icone **+** en haut à gauche d'Animateam.
   2. Sélectionnez *Inviter des personnes*.
   3. Remplissez l'adresse e-mail et le nom de la personne.
   4. Cliquez sur *Copier le lien d’invitation*.
   5. Envoyez l'URL copiée à la personne que vous souhaitez inviter.

## Exclure un utilisateur de l'équipe

 Pour exclure un utilisateur, vous devez être administrateur et suivre ces étapes :

   1. Cliquez sur le menu principal d'Animateam.
   2. Sélectionnez *Gérer les membres*.
   3. Recherchez l'utilisateur dans la liste.
   4. Cliquez sur son rôle (par exemple, *Membre*) à l'extrémité de la ligne.
   5. Cliquez sur *Exclure de l'équipe*.

## Effectuer une recherche incluant des paramètres

   Pour effectuer une recherche incluant des paramètres :

   1. Cliquez sur la barre de recherche au-dessus de la discussion.
   2. Sélectionnez *Messages* ou *Fichiers*.
   3. Choisissez une ou plusieurs options de recherche.
   4. Ajoutez l'objet de la recherche.

   Votre recherche peut ressembler à "in:town-square terme à rechercher" ou
  "from:almichel in:chatschiensautresanimaux terme à rechercher".

## (Dés)activer les notifications par e-mail

1. Cliquez sur le bouton *Paramètres* situé en haut à droite.
2. Dans *Notifications*, cliquez sur *Notifications par e-mail*.
3. Vous avez également la possibilité de gérer les notifications de bureau et
   les notifications mobiles.

## Autres ressources

Vous pouvez également consulter la [documentation de Framateam](http://docs.framateam.org/), 
projet sur lequel est basé Animateam. La plupart des informations fournies devraient également s'appliquer à Animateam.
