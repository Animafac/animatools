# [![Animadrive](resources/drive/logo.svg)](https://drive.animafac.net/)

## Partage de fichiers

### Synchroniser ses fichiers

#### Synchroniser ses fichiers sur son ordinateur

On va utiliser pour cela le logiciel [ownCloud](https://owncloud.org/download/#owncloud-desktop-client).
(Vous devez télécharger la version *production* d'*ownCloud desktop client*
pour votre système d'exploitation.)

Au premier lancement du logiciel, il faut configurer la connexion au serveur :

1. Dans le champ *Adresse du serveur*, renseignez `https://drive.animafac.net/`.
1. Indiquez ensuite votre identifiant et votre mot de passe Animadrive.
1. Choisissez si vous voulez synchroniser tout votre Drive
    ou juste un dossier en particulier.
1. Choisissez un dossier local dans lequel les fichiers seront importés depuis Animadrive.

Une fois la configuration effectuée,
le logiciel va tourner en tâche de fond et synchroniser les fichiers.
Chaque fichier ajouté/modifié/supprimé dans votre dossier Animadrive
le sera également sur le serveur et vice-versa.

##### Lancer le logiciel ownCloud au démarrage de l'ordinateur

Par défaut, ownCloud ne se lance pas automatiquement au démarrage de votre ordinateur,
les fichiers ne seront donc pas synchronisés si vous ne le lancez pas.

Si vous voulez qu'il se lance automatiquement :

1. Faites un clic droit sur l'icône ownCloud dans votre barre des tâches.
1. Cliquez sur *Paramètres*.
1. Cliquez sur l'onglet *Généraux*.
1. Cochez la case *Lancer au démarrage du système*.
1. Cliquez sur *Fermer*.

##### Des fichiers avec « conflicted copy » dans leur nom apparaissent sur mon ordinateur

Cela peut survenir lorsque quelqu'un a modifié un fichier en même temps que vous.
ownCloud se retrouve alors avec deux versions du même fichier
et privilégie toujours celle qui est sur le serveur.
Mais afin de ne pas perdre l'autre version,
elle est copiée localement et renommée
(avec « conflicted copy » et la date du conflit ajoutés dans le nom du fichier).

C'est alors à vous de comparer les deux fichiers
afin de voir quelle version vous souhaitez garder.
Transférez le contenu que vous souhaitez garder dans le fichier original ;
vous pouvez ensuite supprimer la version « conflicted copy »
afin d'ownCloud sache que le conflit est résolu.

#### Accéder à ses fichiers sur Android

Pour cela, on a besoin de l'application [Nextcloud](https://play.google.com/store/apps/details?id=com.nextcloud.client).

Une fois l'application installée :

1. Lancez là.
1. Dans le champ *Adresse du serveur*, renseignez `https://drive.animafac.net/`.
1. Indiquez ensuite votre identifiant et votre mot de passe Animadrive.
1. Validez.

L'application vous permet ensuite d'accéder à tous vos fichiers sur Animadrive.

**Note** : L'application ne gère pas directement l'édition de fichiers.
Si vous souhaitez éditer des documents,
il vous faudra installer une application adaptée selon le type de document.

#### Transférer un dossier depuis Google Drive

Le plus simple pour de gros dossiers est d'utiliser l'outil de synchronisation
(cf. [Synchroniser ses fichiers sur son ordinateur](#synchroniser-ses-fichiers-sur-son-ordinateur)).

Une fois la synchronisation configurée sur votre ordinateur :

1. Sur Google Drive, faites un clic droit sur le dossier à transférer
    et sélectionnez *Télécharger*.
    Google Drive va alors vous proposer de télécharger
    une archive contenant tout le dossier
    (cela peut prendre plusieurs minutes s'il y a beaucoup de fichiers).
1. Décompressez l'archive téléchargée.
1. Copiez le contenu du dossier décompressé dans votre dossier synchronisé.

Attendez qu'ownCloud synchronise les fichiers en fond
(cela peut prendre longtemps si votre connexion est lente).

### Partager un dossier avec quelqu'un qui n'a pas de compte Animadrive

1. Cliquez sur l'icône de partage (![Icône Partager](resources/drive/screenshots/share_icon.png))
    à côté du nom du dossier.
1. Cochez la case *Partager par lien public*.
1. Copiez l'URL qui apparaît
    et envoyez-là à la personne à laquelle vous voulez donner accès au dossier.

### Partager un document

#### Partager un document en lecture seule

1. Cliquez sur l'icône de partage (![Icône Partager](resources/drive/screenshots/share_icon.png))
    à côté du nom du document.
1. Cochez la case *Partager par lien public*.
1. Copiez l'URL qui apparaît
    et envoyez-là à la personne à laquelle vous voulez donner accès au document.

Cette URL publique permet uniquement de télécharger le document et pas de le modifier
(même si la personne possède un compte Animadrive).

#### Partager un document en écriture

1. Cliquez sur l'icône de partage (![Icône Partager](resources/drive/screenshots/share_icon.png))
    à côté du nom du document.
1. Dans le champ qui s'affiche,
    tapez le nom de la personne à laquelle vous voulez partager le document.
1. Cochez la case *Notifier par courriel*
    si vous souhaitez que la personne reçoive un lien vers le document.

Il n'y a pas de bouton *Valider*, les nouveaux partages sont automatiquement sauvegardés.

## Suite bureautique

### Activer la vérification orthographique

Par défaut, les nouvaux documents n'ont pas de langue.
Il arrive également que la langue d'un document soit mal détectée.
(*Allemand* est alors affiché dans la barre du bas
mais en réalité aucune langue n'est sélectionnée.)

Pour activer la vérification du français :

1. Dans le menu, cliquez sur *Outils > Langue > Pour tout le texte > Français (France)*.
1. Assurez-vous qu'*Outils > Correction orthographique automatique* est bien coché.

### Un de mes documents a une icône grise et Animadrive n'arrive plus à l'ouvrir

Cela arrive généralement lorsqu'en renommant un fichier,
vous enlevez l'extension (la partie après le point dans le nom du fichier)
et le serveur ne sait alors plus de quel type de fichier il s'agit.

La solution est alors de renommer le fichier et d'ajouter la bonne extension :

* Pour un document texte créé avec Animadrive ou LibreOffice : `.odt`
* Pour un document tableur créé avec Animadrive ou LibreOffice : `.ods`
* Pour un document texte créé avec Microsoft Word : `.docx`
* Pour un document tableur créé avec Microsoft Excel : `.xlsx`

Vous trouverez d'autres extensions couramment utilisées sur [cette page Wikipédia](https://fr.wikipedia.org/wiki/Extension_de_nom_de_fichier#Exemples_d'extensions_courantes_(Pour_windows)).

## Répertoire de contacts

### Gérer ses contacts

Pour accéder à vos contacts,
il faut utiliser l'application Contacts (dans le menu en haut à gauche) :
![Application Contacts dans le menu des applications](resources/drive/screenshots/contacts_app.png)

#### Partager un carnet d'adresses avec d'autres utilisateurs

Pour cela, on va d'abord créer un nouveau carnet d'adresses :

1. Connectez vous à [Animadrive](https://drive.animafac.net/).
1. Ouvrez l'application Contacts (via le menu en haut à gauche).
1. Cliquez sur *Paramètres* tout en bas à gauche.
1. Renseignez le champ *Nom du carnet d'adresses* puis validez en cliquant sur *➡*.
    Le carnet apparaît dans la liste des carnets d'adresses juste au dessus.
1. Cliquez sur l'icône *Partager* (![Icône Partager](resources/drive/screenshots/share_icon.png))
    à côté du nom du carnet.
1. Renseignez dans le champ qui apparaît en dessous
    le nom d'utilisateur avec lequel partager le carnet.

### Synchroniser ses contacts

#### Synchroniser ses contacts avec Android

Pour cela, on a besoin de l'application [Open Sync](https://play.google.com/store/apps/details?id=com.deependhulla.opensync).

Une fois l'application installée :

1. Lancez là.
1. Cliquez sur le bouton *+* en bas à gauche.
1. Sélectionnez *Connexion avec une URL et un nom d'utilisateur*.
1. Dans le champ *URL de base*, indiquez l'URL suivante : `https://drive.animafac.net/remote.php/dav/`.
    Renseignez également votre nom d'utilisateur et votre mot de passe Animadrive
    puis cliquez sur *Se connecter*.
1. Une fois la connexion établie, cliquez sur *Créer un compte*.

**Note** : tous vos contacts ne sont pas automatiquement synchronisés,
seulement ceux rattachés au compte Open Sync.
Quand vous créez ou modifiez un nouveau contact sur votre téléphone,
vous pouvez choisir à quel compte le rattacher.

**Note** : cela synchronisera également vos agendas.

##### Open Sync n'affiche pas d'erreur mais mes contacts ne se synchronisent pas

Tout d'abord, vérifiez que l'application Open Sync
a bien la permission d'accéder à vos contacts.
[Cet article](https://support.google.com/googleplay/answer/6270602?hl=fr)
explique comment contrôler les permissions des applications.

Si ça ne suffit pas,
ouvrez le menu principal de l'application Contacts et sélectionnez *Contacts à afficher*.
Il faut que soit *Tous les contacts*, soit Open Sync
soit sélectionné pour que vos contacts synchronisés apparaisse.

#### Synchroniser ses contacts avec Thunderbird

Vous pouvez installer l'extension [CardBook](https://addons.mozilla.org/fr/thunderbird/addon/cardbook/)
pour Thunderbird.

Avant de pouvoir utiliser l'extension,
il va falloir récupérer l'URL de votre carnet d'adresse :

1. Connectez vous à [Animadrive](https://drive.animafac.net/).
1. Ouvrez l'application Contacts (via le menu en haut à gauche).
1. Cliquez sur *Paramètres* tout en bas à gauche.
1. Dans le menu déroulant (trois petits points en dessous de *Paramètres*),
    sélectionnez *Lien*.
1. Copiez le lien qui s'affiche.

Une fois l'extension installée :

1. Ouvrez CardBook (*Outils > CardBook* dans le menu de Thunderbird).
1. Cliquez sur *Carnet d'adresses > Nouveau carnet d'adresses* dans le menu de CardBook.
1. Sélectionnez *Distant* comme emplacement du carnet puis cliquez sur *Suivant*.
1. Choisissez *CardDAV* comme type de carnet
    et indiquez l'URL du carnet récupérée précédemment dans le champ *URL*.
    Renseignez également votre nom d'utilisateur et votre mot de passe Animadrive
    puis cliquez sur *Valider*.
1. Une fois la validation terminée, cliquez sur suivant.
1. Choisissez un nom et une couleur pour votre carnet d'adresses,
    puis cliquez sur *Suivant*.

## Agenda partagé

### Gérer ses agendas

Pour accéder à vos agendas,
il faut utiliser l'application Agenda (dans le menu en haut à gauche) :
![Application Agenda dans le menu des applications](resources/drive/screenshots/agenda_app.png)

#### Importer un agenda depuis Google Agenda

Il y a deux manières d'intégrer un agenda Google dans Animadrive.
Vous pouvez simplement l'afficher mais continuer à le gérer dans Google Agenda
ou bien transférer tout son contenu pour ensuite le gérer entièrement dans Animadrive.

##### Afficher un calendrier Google Agenda dans Animadrive

**Note** : cet agenda ne sera disponible qu'en lecture seule.

Il faut tout d'abord récupérer l'URL de l'agenda dans Google Agenda :

1. Cliquez sur le menu déroulant à côté du nom de l'agenda
    et sélectionnez *Paramètres de l'agenda*.
1. Cliquez sur *Adresse URL privée* > *ICAL* puis copiez l'URL fournie.

Ensuite, dans Animadrive :

1. Dans *Agenda*, cliquez sur *Nouvel abonnement*.
1. Renseignez l'URL de l'agenda que vous avez copiée avant et cliquez sur *Créer*.

##### Importer le contenu d'un calendrier Google Agenda dans Animadrive

**Note** : il s'agit d'un transfert,
l'agenda ne sera pas ensuite synchronisé entre Animadrive et Google Agenda.

Il faut tout d'abord récupérer un fichier contenant tous vos événements :

1. Cliquez sur le menu déroulant à côté du nom de l'agenda
    et sélectionnez *Paramètres de l'agenda*.
1. Cliquez sur *Exporter cet agenda*.
1. Décompresser le fichier ZIP téléchargé.

Vous devriez alors vous retrouver avec un fichier en `.ics`
contenant l'intégralité de votre agenda.

Ensuite, dans Animadrive :

1. Dans *Agenda*, cliquez sur *Settings & Import*.
1. Cliquez sur *Importer un agenda*.
1. Sélectionnez le fichier `.ics` téléchargé précédemment.

#### Partager un agenda avec un autre utilisateur d'Animadrive

1. Cliquez sur l'icône de partage à côté du nom de l'agenda.
1. Entrez le nom de l'utilisateur en question.

#### Quand quelqu'un m'invite à un événement, je reçois un e-mail étrange

Si l'e-mail contient du code commençant par `BEGIN:VCALENDAR`,
c'est que votre lecteur d'e-mails ne sait pas afficher les événements.

Si vous utilisez Thunderbird, vous avez besoin d'installer l'extension [Lightning](https://addons.mozilla.org/fr/thunderbird/addon/lightning/).

#### Dupliquer un événement

Il n'est pas possible de directement copier un événement
mais il existe un moyen détourné :

1. Cliquez sur l'événement.
1. Cliquez sur *Plus…*.
1. Cliquez sur *Exporter*.
1. Vous obtenez un fichier à enregistrer sur votre disque dur.
1. Dans la partie agenda d'Animadrive, cliquez sur *Settings & Import*.
1. Cliquez sur *Importer un agenda*.
1. Sélectionnez le fichier précédemment téléchargé.
1. Sélectionner l'agenda dans lequel importer l'événement.
1. Validez.

### Synchroniser ses agendas

#### Synchroniser ses agendas avec Android

Pour intégrer les calendriers Animadrive dans l'application Agenda d'Android,
il est possible d'utiliser l'application [Open Sync](https://play.google.com/store/apps/details?id=com.deependhulla.opensync).

La procédure est alors la même que pour [synchroniser ses contacts](#synchroniser-ses-contacts-avec-android).

##### Open Sync n'affiche pas d'erreur mais mes agendas ne se synchronisent pas

Tout d'abord, vérifiez que l'application Open Sync
a bien la permission d'accéder à vos agendas.
[Cet article](https://support.google.com/googleplay/answer/6270602?hl=fr)
explique comment contrôler les permissions des applications.

Si ça ne suffit pas,
ouvrez le menu principal de l'application Agenda
et vérifiez que votre Agenda Animadrive est bien dans la liste
et que la case à côté de son nom est cochée.

#### Synchroniser ses agendas avec Thunderbird

Il vous faudra le module [Lightning](https://addons.mozilla.org/fr/thunderbird/addon/lightning/)
pour Thunderbird.

Ensuite il faut récupérer l'URL de l'agenda
dans Animadrive :

1. Connectez vous à [Animadrive](https://drive.animafac.net/).
1. Ouvrez l'application Agenda (via le menu en haut à gauche).
1. Dans le menu déroulant (trois petits points à côté du nom de l'agenda),
    sélectionnez *Lien*.
1. Copiez le lien qui s'affiche.

Ensuite dans Lightning :

1. Faites un clic droit sur la liste des agendas.
1. Cliquez sur *Nouvel agenda*.
1. Sélectionnez *Sur le réseau* comme emplacement de l'agenda
    puis cliquez sur *Suivant*.
1. Sélectionner *CalDAV* comme format d'agenda
    et indiquez l'URL de l'agenda récupérée précédemment dans le champ *Emplacement*.
    Puis cliquez sur *Suivant*.
1. Choisissez un nom et une couleur pour votre agenda et cliquez sur *Suivant*.

## Administration

### Créer un.e utilisateur.rice

Pour créer un.e utilisateur.rice,
votre compte doit être administrateur ou administrateur de groupe.
(Vous pourrez alors uniquement créer des utilisateur.rice.s dans ce groupe.)

1. Cliquez sur votre nom/pseudo en haut à droite.
1. Cliquez sur *Utilisateurs*.
1. En haut de la liste des utilisateurs, remplissez les champs *Nom d'utilisateur*,
    *Mot de passe* et *E-mail*.
    Sélectionnez également les groupes auxquels vous souhaitez ajouter l'utilisateur.rice.
1. Cliquez sur *Créer*.

## Autres ressources

Vous pouvez également consulter la [documentation de Framadrive](https://docs.framasoft.org/fr/nextcloud/),
projet similaire à Animadrive. Certaines fonctionnalités peuvent néanmoins être différentes.
