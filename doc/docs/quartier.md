# Le Quartier

Cette documentation s'adresse aux salarié.e.s et volontaires d'Animafac.
La documentation à destination des associations du réseau
se trouve sur la page *[Aide](https://lequartier.animafac.net/aide/)* du Quartier.

## Gérer les thématiques

Seuls les comptes avec le rôle *Éditeur* ou *Administateur*
peuvent modifier les thématiques
(pour éviter que les volontaires n'en rajoutent par erreur).

(Les établissements et territoires peuvent par contre être créés
par n'importe quel compte qui a accès à CiviCRM.)

### Ajouter une thématique

Il est possible d'ajouter dans l'administration :

1. Cliquez sur *CiviCRM* dans le menu de gauche.
1. Cliquez sur *Contacts > Gérer les étiquettes* dans la barre du haut.
1. Cliquez sur *Thématiques* dans la liste de gauche.
1. Cliquez sur *Add Child*.
1. Remplissez le champ *Nom*.
1. Cliquez sur *Enregistrer*

Attention : toute nouvelle thématique apparaît automatiquement
dans l'annuaire de la partie association,
essayez donc de ne pas dépasser une quinzaine de thématiques
pour garder l'annuaire utilisable.

### Fusionner des thématiques

Il peut être utile de fusionner deux thématiques
si jamais vous en avez créé une par erreur lors qu'elle existait déjà
ou si vous pensez qu'il y en a trop.

Pour cela :

1. Cliquez sur *CiviCRM* dans le menu de gauche.
1. Cliquez sur *Contacts > Gérer les étiquettes* dans la barre du haut.
1. Cliquez sur la première thématique à fusionner dans la liste de gauche.
1. Tout en maintenant la touche contrôle de votre clavier,
    cliquez sur la deuxième thématique à fusionner.
1. Cliquez sur *Merge Tags*.

Les associations de chacune des deux thématiques
se retrouveront alors dans une thématique commune.

(La même méthode peut être utilisée pour fusionner des établissements ou des territoires.)

## Exporter une liste de contacts

Une fois dans l'administration du Quartier :

1. Cliquez sur *CiviCRM* dans le menu de gauche.
1. Cliquez sur *Rechercher > Recherche avancée* dans la barre du haut.
1. Rentrez les critères de votre export
    (ou laissez tous les champs vides pour exporter tous les contacts).
1. Cliquez sur *Rechercher*.
1. Vérifiez que les résultats qui s'affichent correspondent bien à ce que vous vouliez.
1. Dans *Sélection des enregistrements*, sélectionnez *Les X trouvés*.
1. Cliquez sur *Actions > Contact - exporter*.
1. Cliquez sur *Suivant*.

Votre navigateur vous proposera alors de télécharger un fichier CSV
contenant l'ensemble des contacts correspondants à vos critères de recherche.

**Attention** : Si vous utilisez votre export pour un envoi massif d'e-mails,
pensez à ajouter le critère *Exclure : Ne pas envoyer de courriel* à votre recherche
(dans *Critères de base*, juste au dessus d'*Origine du contact*).

## Sauvegarder une recherche avancée

Il est possible de sauvegarder une recherche avancée
pour pouvoir la relancer plus tard
sans avoir à rentrer de nouveau tous les critères de recherche.

Pour cela, on va créer un groupe dynamique
qui sera mis à jour automatiquement en fonction des critères de votre recherche.

Une fois dans l'administration du Quartier :

1. Cliquez sur *CiviCRM* dans le menu de gauche.
1. Cliquez sur *Rechercher > Recherche avancée* dans la barre du haut.
1. Rentrez les critères de votre export
    (ou laissez tous les champs vides pour exporter tous les contacts).
1. Cliquez sur *Rechercher*.
1. Vérifiez que les résultats qui s'affichent correspondent bien à ce que vous vouliez.
1. Dans *Sélection des enregistrements*, sélectionnez *Les X trouvés*.
1. Cliquez sur *Actions > Groupe - créer un groupe dynamique*.
1. Renseignez le champ *Nom*
    avec un nom vous permettant de retrouver facilement ce groupe.
1. Cliquez sur *Enregistrer ce groupe dynamique*.

Ensuite, pour relancer la même recherche :

1. Cliquez sur *CiviCRM* dans le menu de gauche.
1. Cliquez sur *Rechercher > Rechercher des contacts* dans la barre du haut.
1. Dans le champ *dans*, sélectionnez votre groupe dynamique.
1. Cliquez sur *Rechercher*.

## Afficher une alerte sur le tableau de bord

Il est possible d'afficher une notification
sur le tableau de bord de tou.te.s les utilisateur.rice.s
(pour annoncer une nouvelle fonctionnalité ou un événement important, par exemple).

Pour cela, une fois dans l'administration du Quartier :

1. Cliquez sur *Pages* dans le menu de gauche.
1. Cliquez sur la page nommée *Tableau de bord*.
1. En bas de la page, remplissez le champ *Contenu de l'alerte*.
1. Cliquez sur *Mettre à jour* (en haut à gauche).

Pensez à retirer l'alerte quand elle n'est plus pertinente
(videz simplement son contenu et mettez à jour).

## Indiquer qu'une association a adhéré à Animafac

1. Ouvrez la [fiche d'Animafac](https://lequartier.animafac.net/wp/wp-admin/admin.php?page=CiviCRM&q=civicrm%2Fcontact%2Fview&reset=1&cid=1)
    dans CiviCRM.
1. Cliquez sur l'onglet *Relations*.
1. Cliquez sur *Ajouter une relation*.
1. Dans *Type de relation*, sélectionnez *a comme association adhérente*.
1. Dans *Contact(s)*, sélectionner l'association en question.
1. Si vous souhaitez que l'adhésion expire automatiquement, entrez une date de fin.
1. Cliquez sur *Enregistrer la relation*.

Note : les associations qui ne sont pas marquées comme adhérentes
verront un avertissement sur leur tableau de bord leur demandant d'adhérer.

## Faire en sorte que les coordonnées des ARX s'affichent sur le tableau de bord

Il est possible d'afficher sur le tableau de bord un bloc *Mon ou ma référent.e Animafac*
qui contient le numéro de téléphone et l'adresse e-mail
du ou des ARX du département de l'association de l'utilisateur.rice.

Pour que le tableau de bord puisse récupérer les bons coordonnées,
il faut associer la fiche de chaque ARX à son département.
Pour cela, on va utiliser des relations de type *ARX*.

Pour ajouter une nouvelle relation de ce type :

1. Ouvrez la [fiche d'Animafac](https://lequartier.animafac.net/wp/wp-admin/admin.php?page=CiviCRM&q=civicrm%2Fcontact%2Fview&reset=1&cid=1)
    dans CiviCRM.
1. Cliquez sur l'onglet *Relations*.
1. Cliquez sur *Ajouter une relation*.
1. Dans *Type de relation*, sélectionnez *ARX*.
1. Dans *Contact(s)*, sélectionner l'ARX en question
    (il faut qu'il ait déjà un compte sur le Quartier).
1. Dans la section *ARX*, sélectionnez le département
    pour lequel cet.te ARX doit être référent.e.
1. Cliquez sur *Enregistrer la relation*.

Répétez l'opération pour chaque ARX à ajouter comme référent.e.
Et pensez bien sûr à supprimer les relations des ancien.ne.s ARX.
