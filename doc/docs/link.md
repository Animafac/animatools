# [![Animateam](resources/link/logo.svg)](https://link.animafac.net/)

## Raccourcir un Lien

Pour raccourcir un lien, suivez ces étapes :

1. Accédez à la page d'accueil d'[Animalink](https://link.animafac.net/), où vous trouverez un champ de texte
   dédié à la création de liens courts.
2. Collez le lien long que vous souhaitez raccourcir, que vous pouvez copier
   depuis votre navigateur ou d'une autre source.
3. Vous avez la possibilité de personnaliser le texte du lien pour le rendre plus
   explicite.
4. Appuyez sur le bouton **Raccourcir**. Animalink générera instantanément un lien
   court au format asso.li/votrelien.

## Modification du Texte du Raccourci

Si vous avez commis une erreur dans le texte du raccourci ou souhaitez simplement
le modifier, suivez ces étapes :

1. Accédez à la page d'accueil d'Animalink.
2. Utilisez le champ *Texte du raccourci personnalisé* pour entrer manuellement
   le nouveau texte que vous souhaitez associer au lien.
3. Cliquez sur *Valider* pour mettre à jour le texte du raccourci.

## Gestion des Liens

Si vous avez raccourci plusieurs liens et que vous souhaitez les gérer, voici
comment procéder :

1. Dirigez-vous vers la section *Statistiques* sur la page d'accueil d'Animalink.
   Vous y trouverez une liste de tous les liens que vous avez raccourcis.

2. Vous avez également la possibilité de consulter des statistiques de base
   concernant le nombre de clics générés par chaque lien. Ces statistiques
   peuvent inclure plusieurs clics d'une même personne.

## Supprimer un Lien

Dans la section des *Statistiques*, vous avez la possibilité de supprimer des
redirections existantes. Veuillez noter que cette action est **irréversible** !

## Autres Ressources

Pour plus d'informations, n'hésitez pas à consulter la [documentation de Framalink](https://docs.framasoft.org/fr/lstu/), le projet sur lequel est basé Animalink.
