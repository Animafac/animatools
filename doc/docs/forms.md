# [![Animaforms](resources/forms/logo.svg)](https://forms.animafac.net/admin/)

## Collecter des données personnelles

Si votre formulaire collecte des données personnelles, vous devez respecter
la réglementation européenne et notamment :

- Expliquer comment vont être utilisées les données ;
- Demander un consentement explicite (case à cocher) pour chaque utilisation des données ;
- Fournir un moyen aux utilisateur.rice.s de demander la suppression des données les concernant.

Vous pouvez consulter [cet article sur le RGPD](https://www.helloasso.com/blog/rgpd-et-associations-tout-comprendre-en-5-points-cles/) pour en savoir plus.

## Je ne peux pas ajouter de questions

On ne peut pas directement ajouter une question. En effet, elles doivent être rattachées
à des groupes de questions (en gros les différentes pages du questionnaire).
S'il n'y a pas encore de groupe, il faut donc d'abord cliquer sur **Ajouter un groupe**
avant de pouvoir ajouter des questions.

## Ajouter un bloc de texte

Pour insérer un simple bloc de texte dans le questionnaire, il faut en fait ajouter une question
de type *Affichage de texte* et taper le texte que vous voulez afficher dans le champ *Question*.

Pour ajouter un bloc de texte dans un questionnaire, il faut en fait sélectionner au niveau de *Type de question* une *Questions de type Texte* et taper le texte désiré dans le champ *Question*.

![Exemple de question de type Affichage de texte](resources/forms/screenshots/bloc-texte.png)

## Insérer une image dans un bloc de texte

1. Dans l'éditeur de texte, cliquez sur ![Image](resources/forms/screenshots/image_btn.png).
2. Cliquez sur *Explorer le serveur* ou entrez l'URL de l'image.
3. Cliquez sur *Envoyer*.
4. Sélectionnez votre image.
5. Double-cliquez sur la vignette de l'image qui a été ajoutée dans l'explorateur.
6. Cliquez sur *OK*.

## Ajouter un message d’accueil ou de fin

Pour ajouter un message d’accueil ou de fin au questionnaire, sélectionnez *Éléments de texte*
à gauche du questionnaire. Vous avez la possibilité d’ajouter des éléments tels qu’une description
au formulaire, un message d’accueil et un message de fin.

![Ajout de message de début ou de fin](resources/forms/screenshots/message-form.png)

## Afficher les réponses à un questionnaire

Cliquez sur *Liste de questionnaires*, puis défilez jusqu’au questionnaire choisi.
Ensuite, cliquez sur l’icône *Statistiques* qui se trouve dans la colonne *Action*.

## Importer les réponses dans Google Sheets

Tout d'abord, il vous faut récupérer l'identifiant de votre questionnaire.
Il est affiché après le titre du questionnaire quand vous l'éditez :

![Exemple d'identifiant de questionnaire](resources/forms/screenshots/identifiant-questionnaire.png)

Ensuite, dans le fichier Google Sheet, vous devez insérer comme valeur dans la première cellule A1 :

`=IMPORTDATA("https://tools.animafac.net/limesurvey-export/csv/[ID_QUESTIONNAIRE]?token=[TOKEN]")` 
en remplaçant `[ID_QUESTIONNAIRE]` par l'identifiant du questionnaire et `[TOKEN]` par le token.

Vous obtenez un résultat similaire : `=IMPORTDATA("https://tools.animafac.net/limesurvey-export/csv/99397?token=abc123XYZ456")`.


## Importer les réponses dans OnlyOffice

1. Récupérez l'ID du questionnaire dont vous souhaitez importer les réponses.
2. Construisez l'URL d'import sous le format suivant : `https://tools.animafac.net/limesurvey-export/csv/ID_QUESTIONNAIRE?token=[TOKEN]`  en remplaçant `[ID_QUESTIONNAIRE]` par l'identifiant du questionnaire et `[TOKEN]` par le token.
3. Dans la feuille de calcul OnlyOffice, collez cette URL dans la première cellule A1.
4. Accédez à l'onglet "Module complémentaire" et sélectionnez l'option "Macros".
5. Copiez le code de la macro à partir [de ce lien](https://paste.yunohost.org/nojupusehu.js)
6. Exécutez la macro :
   
   - Revenez à OnlyOffice et collez le code dans l'éditeur de macros.
   - Appuyez sur "Autostart", puis sur "Exécuter".
   - Confirmez l'action en cliquant sur "OK".

Ces étapes permettront d'importer les données du formulaire dans la feuille de calcul active
et de les mettre à jour.

### La première case de mon tableau affiche `#REF!`

Cela se produit quand du texte est entré dans une case importée.
Il suffit d'effacer ce texte et la synchronisation reprendra normalement.
Vous pouvez par contre sans souci ajouter des couleurs ou du style (gras, italique, etc.) 
aux données importées.

### La première case de mon tableau affiche `#N/A`

Cela signifie que l’exportation des données n’a pas pu être effectuée.
Vérifiez que les paramètres de l'url sont corrects et réessayez.
<!--
## Exporter les réponses vers Animadrive

 Tout d'abord, il vous faudra générer un mot de passe permettant de relier Animaforms à Animadrive. 
Pour cela, dans Animadrive :
1. Cliquez sur votre nom en haut à droite.
2. Cliquez sur *Personnel*.
3. Cliquez sur *Mots de passe d'applications*.
4. Dans *Nom de l'application*, indiquez *Animaforms*.
5. Cliquez sur *Créer un nouveau mot de passe d'application*.
6. Enregistrez quelque part le mot de passe qui apparaît dans le champ *Mot de passe*. -->


## Changer le thème d'un formulaire
Animaforms vous permet de personnaliser l'apparence de vos formulaires en modifiant leur thème. 
Un thème détermine l'apparence générale du formulaire, en contrôlant des éléments tels que les couleurs, les polices, et la mise en page.
#### Appliquer un thème existant
1. Dans l'interface d'administration d'Animaforms, accédez à la *Liste des questionnaires* 
et cliquez sur le questionnaire que vous souhaitez modifier.
2. Une fois dans le questionnaire, cliquez sur l'onglet *Paramètres généraux*.
3. Dans la liste déroulante des thèmes disponibles, sélectionnez celui que vous souhaitez appliquer.
4. Cliquez sur *Sauvegarder* pour appliquer le thème au questionnaire.
![Appliquer un thème existant](resources/forms/screenshots/application-theme-form.png)

#### Créer ou modifier un thème personnalisé

Si vous souhaitez créer un nouveau thème ou modifier un thème existant en utilisant du CSS :

1. Dans le menu principal de l'interface d'administration, cliquez sur *Configuration*, puis sur *Thèmes*.
![Congigurer theme formulaire](resources/forms/screenshots/configuration-theme-form.png)

2. Une liste des thèmes de questionnaires installés vous est proposée. 
Pour créer un nouveau thème à partir d'un existant, cliquez sur le bouton *Copier* du thème souhaité, 
donnez un nom au nouveau thème, puis accédez à l'éditeur.
![Choix theme](resources/forms/screenshots/copier-theme-form.png)

3. Pour modifier un thème existant, cliquez directement sur *Éditeur de thème*.
![Editeur theme](resources/forms/screenshots/editeur-theme-form.png)

4. Dans l'éditeur de thèmes, sélectionnez le fichier custom.css pour ajouter votre propre 
code CSS et personnaliser les éléments de style du formulaire, tels que les couleurs ou les marges.
![Ajout css](resources/forms/screenshots/ajout-css-theme.png)

5. Après avoir effectué les modifications, enregistrez votre thème. Utilisez l'aperçu en bas 
de l'éditeur pour voir les modifications apportées. Vous pouvez également prévisualiser 
l'affichage du formulaire sur différentes tailles d'écran (mobile, 640 x 480, 1024 x 768).
![Previsualiser theme](resources/forms/screenshots/appercu-form.png)

6. Une fois votre thème prêt, appliquez-le à un questionnaire en suivant les étapes décrites 
dans la section *Appliquer un thème existant*.

#### Exemple de personnalisation via CSS
Voici un exemple de code CSS que vous pouvez utiliser pour personnaliser les boutons de votre formulaire :

![css personnalisé](resources/forms/screenshots/css-exemple.png)


## Quand vous éditez votre questionnaire 
1. Cliquez sur *Extensions simples* dans le menu de gauche.
2. Cliquez sur *Paramètres de l'extension LSNextCloud*.
3. Dans *Nom d'utilisateur NextCloud*, indiquez votre nom d'utilisateur Animadrive.
4. Dans *Mot de passe NextCloud*, indiquez le mot de passe Animadrive.
5. Cliquez sur *Sauvegarder*.

Une fois ce réglage en place, les nouvelles réponses au questionnaire seront enregistrées 
dans un fichier Excel éponyme sur Animadrive. Par défaut, le fichier Excel sera créé à la racine 
de votre espace Animadrive. 
<!-- Si vous souhaitez qu'il soit placé ailleurs, vous pouvez spécifier 
un chemin (`/Staff/Projets/Animation de réseau/`, par exemple) dans le champ 
*Chemin du dossier sur LNextCloud*. Ce dossier doit déjà exister sur Animadrive. -->

Si quand vous complétez le questionnaire, le fichier n'apparaît pas sur Animadrive, 
c'est probablement qu'un des paramètres n'est pas correct.

## Les différents types de question

Il existe plusieurs types de questions disponibles, et il peut être facile de s'y perdre. 
Voici les principaux types dont vous aurez besoin :

 -Zone de texte court :
  ![Exemple de question de type Zone de texte court](resources/forms/screenshots/short_text.png)
  &nbsp;
 -Zone de texte long :
  ![Exemple de question de type Zone de texte long](resources/forms/screenshots/long_text.png)
  &nbsp;
 -Choix multiples :
  ![Exemple de question de type Choix multiples](resources/forms/screenshots/multiple_choices.png)
  &nbsp;
 Liste :
  ![Exemple de question de type Liste](resources/forms/screenshots/list.png)

Il existe également des questions prédéfinies qui peuvent vous faire gagner du temps en évitant d'avoir 
à ajouter manuellement les différentes réponses possibles :

 -Oui/Non :
  ![Exemple de question de type Oui/Non](resources/forms/screenshots/yes_no.png)
  &nbsp;
 -Genre :
  ![Exemple de question de type Genre](resources/forms/screenshots/gender.png)
  &nbsp;
 -Entrée numérique :
  ![Exemple de question de type Entrée numérique](resources/forms/screenshots/number.png)
  &nbsp;
 -Date et heure :
  ![Exemple de question de type Date et heure](resources/forms/screenshots/date.png)
  &nbsp;

## Définir les réponses possibles à une question

Pour une question de type *Choix multiples*, cliquez sur *Éditer les sous-questions* 
lorsque vous êtes sur la page *Résumé de la question*.

Pour une question de type *Liste*, cliquez sur *Éditer les options de réponse*
lorsque vous êtes sur la page *Résumé de la question*.

## Imposer un format spécifique aux réponses

Pour un champ de type *Zone de texte court*, il peut être utile de s'assurer 
que les utilisateur.rice.s respectent bien un format de réponse particulier 
(un numéro de téléphone, uneadresse e-mail, etc.).

Pour cela, vous pouvez remplir le champ *Validation* (dans *Options générales*, à droite) 
lorsque vous éditez la question. 
Ce champ utilise des [*expressions régulières*](https://fr.wikipedia.org/wikExpression_r%C3%A9guli%C3%A8re), c'est-à-dire un code permettant de décrire quels caractères sont acceptés.

Voici ce que vous pouvez entrer dans le champ validation pour des besoins courants :

 **Une adresse e-mail :** `/^.+?@.+?\..+$/`
 **Un numéro de téléphone français :** `/^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$/`

(**Attention à bien copier l'ensemble du code tel quel.**)

D'autres expressions de validation sont disponibles [dans la documentation de LimeSurvey](https://manual.limesurvey.org/Using_regular_expressions).

## Définir des conditions

Si vous souhaitez qu'une question ou un bloc de texte ne s'affiche que sous certaines conditions 
(par exemple si on a répondu *Oui* à la question précédente), vous devez cliquer sur 
** Fixer les conditions ** lorsque vous êtes sur la page *Résumé de la question*.

L'éditeur de conditions permet de créer des scénarios poussés mais peut être trop complexe 
pour un usage basique. Il y a par contre un bouton *Ajout rapide de conditions* qui permet d'ajouter facilement des conditions de type « Afficher la question si la réponse à la question *X* est *Y* ».

Par exemple, ici on affiche la question uniquement si l'association n'est pas adhérente :
![Exemple de condition](resources/forms/screenshots/conditions.png)

## Modifier l'ordre des questions

1. Cliquez sur ***Organisateur de question*** dans le menu de gauche.
2. Glissez les questions pour les réorganiser.
3. Cliquez sur *Sauvegarder*.

## Ajouter une URL de fin

Vous pouvez rediriger les utilisateurs vers un autre site à la fin du questionnaire. 
Pour cela :

1. Allez dans *Propriétés de questionnaire* > *Paramètres généraux et texte*.
2. Remplissez le champ *URL de fin*.
3. Cliquez sur *Sauvegarder*.

## Intégrer la billetterie HelloAsso

1. Dans l'interface de HelloAsso, allez dans l'onglet *Diffusion* lorsque vous administrez 
   votre billetterie.
3. Copiez le code fourni dans *Intégrer un widget de votre campagne sur votre site internet* > *Le formulaire* (il commence par `<iframe id="haWidget"`).
4. Dans Animaforms, ajoutez une question de type *Affichage de texte*.
5. Dans l'éditeur de texte, cliquez sur ![Source](resources/forms/screenshots/source_btn.png).
6. Collez le code fourni par HelloAsso dans l'éditeur.
7. Cliquez de nouveau sur *Source*.
8. Cliquez sur *Sauvegarder*.

## Dupliquer un questionnaire

1. Dans l'administration d'Animaforms, cliquez sur *Créer un questionnaire*.
2. Cliquez sur l'onglet *Copier*.
3. Sélectionnez le questionnaire à copier dans la liste déroulante.
4. Cliquez sur *Copier un questionnaire*.

## Permettre à d'autres personnes d'éditer mon questionnaire

1. Dans l'administration du questionnaire, cliquez sur *Menu de questionnaire* > *Permissions du questionnaire*.
2. Ajoutez un nouvel utilisateur ou groupe d'utilisateurs.
3. Cliquez sur *Définir des permissions pour le questionnaire*.
4. Cochez les permissions que vous voulez attribuer à cet utilisateur ou ce groupe d'utilisateurs.
5. Cliquez sur *Sauvegarder*.

## Autres ressources

Vous pouvez consulter la [documentation de LimeSurvey](https://manual.limesurvey.org/LimeSurvey_Manual/fr), l'outil sur lequel Animaforms est basé.
