# ANIMAMEMO
<!-- # [![Animamémo](<link_to_logo>)](<link_to_website>) -->

## Créer et Organiser des Tableaux Collaboratifs

Animez vos sessions de brainstorming grâce [ANIMAMEMO](https://memo.animafac.net/) 
qui permet de travailler en temps réel avec d'autres utilisateurs sur le même tableau.

## Création de Tableaux

![Ajouter un tableau](resources/memo/screenshot/creation-tableau.png)

Vous pouvez créer des tableaux sans ouvrir de compte. Pour commencer, 
nommez votre tableau puis cliquez sur le bouton **Allons-y**.
  
## Gestion des Notes
 Pour créer une nouvelle note, cliquez sur *Ajouter un Post-it* situé
 dans le coin inférieur gauche. Vous pouvez choisir la couleur de la note 
  ou laisser une couleur au hasard.

![Ajouter une note](resources/memo/screenshot/ajout-note.png)

 - Double-cliquez sur la note pour l'éditer.
 - Glissez-déposez la note pour la déplacer sur le tableau.
 - Cliquez sur **X** qui apparait dans le coin supérieur droit 
 d'une note pour la supprimer.
  
## Gestion des Colonnes

![Ajouter une colonne](resources/memo/screenshot/colonnes.png)
- Cliquez sur **+** ou **-** qui apparaissent à droite du tableau
  pour ajouter ou supprimer une colonne.
- Double-cliquez sur le titre d'une colonne pour l'éditer.

## Gestion des Gommettes

![Ajouter une colonne](resources/memo/screenshot/votes.png)

- Glissez-deposez les gommettes sur les notes pour y ajouter
des votes ou un classement.
- Pour les retirer, glissez la dernière icone grise jusqu'au post-it
  dont vous souhaite supprimer la gommette.

## Participants
![Participants](resources/memo/screenshot/participants.png)

   - Le nombre de paticipants au tableau est indiqué dans le coin inférieur droit du tableau.
   - Chaque participant peut double-cliquer sur son pseudonyme pour l'éditer.

## Partage

Pour partager un tableau, cliquez sur le bouton *Partager* en haut.
Vous avez accès à l'url du tableau que vous pouvez copier et partager.

**Remarque** : Veuillez noter que tous les tableaux créés sont publics, et toute personne
ayant l'URL peut y accéder. Assurez-vous de partager vos tableaux de manière appropriée.
