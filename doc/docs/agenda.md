# [![Animagenda](resources/agenda/logo.svg)](https://agenda.animafac.net/)


## Ajouter un Événement à l'Agenda

1. Dans Gmail, ouvrez l'onglet Agenda en cliquant sur "Agenda"
   situé en haut à droite de l'écran.
2. En haut à gauche de l'interface de l'agenda,
   cliquez sur le bouton "Créer" pour ajouter un nouvel événement à l’agenda.
3. Vous serez dirigé vers une nouvelle fenêtre où vous pouvez saisir
   les informations sur l'événement Commencez par donner un nom à l'événement.
   précisez sa date et son heure de début et de fin. 
   Vous pouvez également ajouter des détails supplémentaires comme l'emplacement
   de l'événement.
4. Si nécessaire, définissez des rappels pour vous avertir de l'événement.
   Vous pouvez choisir d'être rappelé par e-mail, notification pop-up, SMS, etc.
5. Si l'événement implique d'autres personnes,
   ajoutez-les en tant que participants. Cela leur enverra une invitation 
   à l'événement.
6. Profitez des options de personnalisation pour ajouter des détails
   spécifiques à l'événement, tels que la couleur de l'événement, la répétition, 
   ou des liens vers des documents associés.
7. Après avoir saisi toutes les informations, cliquez sur "Enregistrer"
   pour ajouter l'événement à votre agenda.

## Ajouter un Agenda

1. Dans l'application de votre agenda, recherchez l'option "Paramètres" ou
   "Gérer les agendas." Elle est généralement située dans le coin supérieur 
   droit ou gauche.
2. Dans les paramètres, cherchez l'option "Ajouter un agenda"
   ou un équivalent similaire.
   Cliquez sur cette option pour commencer le processus d'ajout d'un nouvel agenda.
3. Vous serez invité à donner un nom à l'agenda et à choisir les couleurs 
   ou les options de partage.
4. Cliquez sur "Enregistrer" pour ajouter l'agenda à votre application.
