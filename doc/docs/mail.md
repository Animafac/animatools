# [![Animamail](resources/mail/logo.svg)](https://webmail.animafac.net/)

## Mail

## Ajouter une Signature dans Gmail

1. Accédez à Gmail et connectez-vous à votre compte si ce n'est pas déjà fait.
2. En haut à droite de la fenêtre, vous verrez une icône en forme d’engrenages.
   Cliquez dessus pour accéder aux paramètres de votre compte.
3. Une fois que vous avez cliqué sur l'icône en forme d'engrenages, un menu
   déroulant apparaîtra. Sélectionnez l'option *Voir tous les paramètres* pour
   accéder à une page de paramètres plus détaillée.
4. Sur la page des paramètres, dans la section intitulée *Général*, faites
   défiler jusqu'à l'option *Signature*.
5. Cliquez sur l'option *Signature* pour accéder à l'éditeur de signature. Vous
   pouvez maintenant créer et personnaliser votre signature. Utilisez cet
   espace pour ajouter le texte souhaité, des images, des liens, ou d'autres
   éléments souhaités.
6. Une fois que vous avez créé la signature selon vos préférences, cliquez sur
   le bouton *Enregistrer les modifications* situé en bas de la page pour
   sauvegarder votre nouvelle signature.

En suivant ces étapes, vous aurez configuré une signature personnalisée qui sera
automatiquement ajoutée à chaque e-mail que vous envoyez depuis votre compte
Gmail.
