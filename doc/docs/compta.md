# Animacompta

## Saisir une recette ou une dépense

Avant de pouvoir saisir des opérations,
vérifiez que vous avez bien renseigné un compte bancaire
dans *Comptabilité* > *Banques & caisse* > *Compte bancaire*.

Vous pouvez ensuite vous rendre dans *Comptabilité* > *Saisie*
pour saisir vos opérations.

Et pour afficher les opérations déjà saisies,
rendez-vous dans *Comptabilité* > *Suivi des opérations*.

## Imprimer un compte de résultat ou un bilan

Animacompta permet de générer automatiquement le compte de résultat
et le bilan de votre exercice à partir de vos saisies.
Pour cela :

1. Cliquez sur *Comptabilité* > *Exercices & projets*.
1. Cliquez sur le document que vous souhaitez afficher.

Vous pouvez maintenant utiliser la fonction *Imprimer* de votre navigateur,
tout ce qui est superflu (menu, couleurs) sera alors retiré de la page
afin de générer un document imprimable.
