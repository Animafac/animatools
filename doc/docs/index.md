# ![Animatools](resources/logo.svg)

Ce site documente l'utilisation des [différents outils](https://tools.animafac.net/)
mis en place par Animafac.

## Signaler un problème

Si vous rencontrez un souci, [vérifiez qu'un incident n'est pas déja déclaré](https://status.animafac.net/), puis envoyez un mail à [numerique@animafac.net](mailto:numerique@animafac.net).
