# Animatools

Dans une démarche de réapproriation de nos outils numériques,
Animafac a mis en place plusieurs services libres inspirés des outils de Framasoft.

Ce dépôt contient la documentation de ces différents outils.

Le dépôt étant plutôt simple, il contient deux branches.

## Installation des dépendances

Pour installer les dépendances, vous aurez besoin de [Yarn](https://yarnpkg.com/fr/).
Lancez simplement cette commande :

```bash
yarn install
```

Cela va automatiquement installer les dépendances et générer la documentation.

[Grunt](https://gruntjs.com/) est également nécessaire
pour lancer certaines tâches automatisées.

## Liste des outils

Le fichier `index.html` contient une liste des outils
proposés par Animafac en HTML statique.

Le framework CSS [Furtive](http://furtive.co/)
est utilisé pour donner un style à la page.
Il est installé automatiquement avec Yarn.

## Documentation

La documentation utilisateur des différents outils
est disponible au format Markdown dans `doc/docs/`.

On utilise ensuite [MkDocs](https://www.mkdocs.org/)
pour convertir la documentation en HTML.
(MkDocs n'a pas besoin d'être installé sur la machine,
Yarn se charge de l'installer dans `python/`).
Il existe une tâche Grunt permettant de lancer facilement MkDocs :

```bash
grunt default
```

Le HTML généré se trouve alors dans `doc/site/`.

Il est possible de vérifier que le Markdown est formaté correctement
avec cette tâche Grunt :

```bash
grunt markdownlint
```

### Documentation administrateur

En plus de la documentation utilisateur,
une documentation administrateur est disponible dans [le wiki](https://framagit.org/Animafac/animatools/wikis/home)
de ce dépôt.

## Conditions générales d'utilisation

Le fichier `CGU.html` contient les conditions d'utilisation en HTML statique.
Le framework CSS Furtive est également utilisé.

Plusieurs services ont un lien dans leur pied de page qui renvoie vers ces CGU.

## Intégration continue

Une tâche [Gitlab CI](https://docs.gitlab.com/ee/ci/) est configurée pour ce dépôt.
Elle lance automatiquement `grunt markdownlint` après chaque push.

## Déploiement

La tâche `grunt prod` utilise [shipit](https://github.com/shipitjs/shipit)
pour se connecter au serveur en SSH,
mettre le code à jour depuis la branche master et générer la documentation.
