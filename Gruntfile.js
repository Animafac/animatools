/*jslint node: true*/
module.exports = function (grunt) {
    'use strict';

    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-mkdocs');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shipit');
    grunt.loadNpmTasks('shipit-git-update');
    grunt.loadNpmTasks('grunt-jsonlint');
    grunt.loadNpmTasks('grunt-fixpack');
    grunt.loadNpmTasks('grunt-markdownlint');
    grunt.loadNpmTasks('grunt-env');

    grunt.initConfig({
        jslint: {
            Gruntfile: {
                src: 'Gruntfile.js'
            }
        },
        mkdocs: {
            doc: {
                src: 'doc/',
                options: {
                    clean: true
                }
            }
        },
        watch: {
            scripts: {
                files: ['doc/docs/*.md'],
                tasks: ['env:python', 'mkdocs']
            }
        },
        shipit: {
            prod: {
                deployTo: '/home/tools/public_html/',
                servers: 'tools@tools.animafac.net',
                postUpdateCmd: 'yarn install --production'
            }
        },
        jsonlint: {
            manifests: {
                src: '*.json',
                options: {
                    format: true
                }
            }
        },
        fixpack: {
            package: {
                src: 'package.json'
            }
        },
        markdownlint: {
            doc: {
                src: ['doc/docs/*.md', 'admin_doc/*.md']
            },
            readme: {
                src: 'README.md'
            }
        },
        env: {
            python: {
                PATH: '../python/bin/',
                PYTHONUSERBASE: '../python/'
            }
        }
    });

    grunt.registerTask('lint', ['jslint', 'jsonlint', 'fixpack', 'markdownlint']);
    grunt.registerTask('default', ['env:python', 'mkdocs']);
    grunt.registerTask('prod', ['shipit:prod', 'update']);
};
